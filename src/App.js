import React, { useState } from 'react'


function App() {
  const [domainSize, setDomainSize] = useState(2);
  const [difficulty, setDifficulty] = useState(2);
  
  const generatePuzzle = () => {
    console.log(domainSize)
    console.log(difficulty)
    return
  }

  return (
    <div>
      <label htmlFor="domainSize">Problem Domain Size:</label>
      <input type="number" id="domainSize" name="domainSize" value={domainSize} onChange={e => setDomainSize(e.target.value)} />
      
      <label htmlFor="difficulty">Difficulty</label>
      <select id="difficulty" 
              name="difficulty" 
              defaultValue={difficulty} 
              onChange={e => setDifficulty(e.target.value)}>
        <option value="1">Easy</option>
        <option value="2">Medium</option>
        <option value="3">Hard</option>
        <option value="4">Extreme</option>
      </select>

      <button onClick={generatePuzzle}>Generate!</button>
    </div>
  );
}

export default App;
